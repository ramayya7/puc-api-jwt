package com.digispice.puc.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import com.digispice.puc.entity.WhiteListMsisdn;


public interface WhiteListMsisdnRepository extends JpaRepository<WhiteListMsisdn, Long>  {
    
	@Query("SELECT u FROM WhiteListMsisdn u where u.msisdn = :msisdn")
	List<WhiteListMsisdn> findMsisdn(@Param("msisdn") Long msisdn);
	
    @Transactional
    int deleteByMsisdn(@Param("msisdn") Long msisdn);
    
    int countByMsisdn(@Param("msisdn") Long msisdn);
   
    public boolean existsById(@Param("msisdn") Long msisdn);
    
    
}
