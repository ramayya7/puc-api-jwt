package com.digispice.puc.main;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import com.digispice.puc.config.AuthenticationManagerProvider;
import com.digispice.puc.config.AuthorizationServerConfiguration;
import com.digispice.puc.config.FilterConfig;
import com.digispice.puc.config.PersistenceJpaConfig;
import com.digispice.puc.config.SecurityConfig;
import com.digispice.puc.config.ServiceConfig;
import com.digispice.puc.config.WebConfig;

@SpringBootApplication

@Import({ 
    
    PersistenceJpaConfig.class,
    WebConfig.class,
    ServiceConfig.class,
    SecurityConfig.class,
    FilterConfig.class,
    AuthenticationManagerProvider.class, 
    AuthorizationServerConfiguration.class
   
})
public class PUCAPI extends SpringBootServletInitializer{

	public static void main(String[] args) {
			SpringApplication.run(PUCAPI.class, args);
	}

}
