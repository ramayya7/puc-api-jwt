package com.digispice.puc.deserializers;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.digispice.puc.dto.WhiteListMsisdnUpdateDto;
import com.digispice.puc.exception.models.BadRequestException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class CustomDeSerializerBeforeUpdate extends StdDeserializer<WhiteListMsisdnUpdateDto> {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private static final long serialVersionUID = 1L;
	
	
	
	private static WhiteListMsisdnUpdateDto whitelistMsisdnUpdateDto =new WhiteListMsisdnUpdateDto();

	protected CustomDeSerializerBeforeUpdate(Class<WhiteListMsisdnUpdateDto> t) {
		super(t);
	}
	@SuppressWarnings("unchecked")
	public CustomDeSerializerBeforeUpdate() {
		this((Class<WhiteListMsisdnUpdateDto>) whitelistMsisdnUpdateDto.getClass());     
	}
	
	public  WhiteListMsisdnUpdateDto deserialize(JsonParser parser, DeserializationContext deserializer) throws IOException {
		try
		{    
			
			
			ObjectMapper objectMapper = new ObjectMapper();

			JsonNode rootNode = objectMapper.readTree(parser);
			
			JsonNode imsi = rootNode.path("imsi");
			
		   if(imsi.textValue()!=null && imsi.textValue()!="" && imsi.textValue().matches("\\d+") )
           {	
			   whitelistMsisdnUpdateDto.setImsi(imsi.asLong());
           }
               
               else if(imsi.textValue()==null || imsi.textValue()=="")
	           {	
            	   whitelistMsisdnUpdateDto.setImsi(Long.valueOf(0));
	           }
	            
	            else if(imsi.textValue()!=null && !(imsi.textValue().matches("\\d+")))
	            {	
	            	 throw new BadRequestException(null, "Bad Request Format");
		        }
           
	            else
	            {
	            	whitelistMsisdnUpdateDto.setImsi(Long.valueOf(0));
	            }

		}

		catch(JsonParseException e)

		{   


			logger.error("Error["+e+"]");
			
		    throw new BadRequestException(null, "Bad Request Format");
		}
		return whitelistMsisdnUpdateDto;
	}



}



