package com.digispice.puc.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import com.digispice.puc.entity.PUCCallDetailsLog;

public interface PUCCallDetailsLogRepository extends JpaRepository<PUCCallDetailsLog, Long>  {
    
	int countByMsisdn(@Param("msisdn") Long msisdn);
    
    
}


