package com.digispice.puc.config;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "com.digispice.puc.service" })
public class ServiceConfig {

    public ServiceConfig() {
        super();
    }
   
    
}
