package com.digispice.puc.interfaces;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

public interface IAuthenticationFacade {

	  Authentication getAuthentication();
	}

	
