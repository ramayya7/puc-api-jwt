package com.digispice.puc.deserializers;
import com.digispice.puc.dto.WhiteListMasterWrapperDto;
import com.digispice.puc.exception.models.BadRequestException;
import com.digispice.puc.utilities.RestPreconditions;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomDeSerializerWrapper extends StdDeserializer<WhiteListMasterWrapperDto> {
	
  private Logger logger = LoggerFactory.getLogger(getClass());
  
  private static final long serialVersionUID = 1L;
  
  private static WhiteListMasterWrapperDto userMasterWrapperDto = new WhiteListMasterWrapperDto();


  protected CustomDeSerializerWrapper(Class<WhiteListMasterWrapperDto> t) { 
	  super(t); 
  }
  
  @SuppressWarnings("unchecked")
  public CustomDeSerializerWrapper() { 
	  this((Class<WhiteListMasterWrapperDto>)userMasterWrapperDto.getClass());
  }

  public WhiteListMasterWrapperDto deserialize(JsonParser parser, DeserializationContext deserializer) throws IOException {
    try 
    {
      
    	ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = (JsonNode)objectMapper.readTree(parser);
      
        if (rootNode.has("msisdn"))
        {
            this.logger.info("Deserializing Delete/GET User Block");
        
             JsonNode msisdn = rootNode.path("msisdn");

             if (msisdn.textValue() == null)
             {

                 throw new BadRequestException(msisdn.textValue(), "Bad Request Format");
             }
        
             if (msisdn.textValue().trim() != null && msisdn.textValue() != "" && msisdn.textValue() != "-1" && msisdn.textValue() != "0")
             {
              RestPreconditions.checkRequestElementIsValid(msisdn.textValue().trim(), "Bad Request Format");
             }

            userMasterWrapperDto.setMsisdn(Long.valueOf(msisdn.asLong()));

      }
 
    }
    catch (JsonParseException e) {
      
    logger.error("Error[" + e + "]");
      
      throw new BadRequestException(null, "Bad Request Format");
    } 

    
    return userMasterWrapperDto;
  }
}
