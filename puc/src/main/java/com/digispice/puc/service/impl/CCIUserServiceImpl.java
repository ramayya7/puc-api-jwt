package com.digispice.puc.service.impl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.digispice.puc.entity.PUCUserDetails;
import com.digispice.puc.repository.PUCUserDetailsRepository;
import com.digispice.puc.service.CCIUserService;

@Service
@Transactional(readOnly=true)
public class CCIUserServiceImpl implements CCIUserService{
	
	protected final Log logger = LogFactory.getLog(getClass());
	@Autowired
	PUCUserDetailsRepository userInfo;
	
	@Override
	public PUCUserDetails findByName(String name) {
		
		return userInfo.findByName(name);
	}
	
	
}
