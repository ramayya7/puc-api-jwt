package com.digispice.puc.entity;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name="tbl_user_puccall_details_log")
public class PUCCallDetailsLog {

		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id")
	    private Long id;
		
	    @Column(name = "usr_msisdn")
	    private Long msisdn;
	
		@Column(name="o_voice_callCount_day")
	    private int o_voice_callCount_day;
		
	    @Column(name="o_voice_callCount_week")
	    private int o_voice_callCount_week;
	    
	    @Column(name="o_voice_callCount_month")
	    private int o_voice_callCount_month;
	    
	    @Column(name="o_voice_callCount_year")
	    private int o_voice_callCount_year;
	    
	    @Column(name="o_sms_count_day")
	    private int o_sms_count_day;
	    
	    @Column(name="o_sms_count_week")
	    private int o_sms_count_week;
	    
	    @Column(name="o_sms_count_month")
	    private int o_sms_count_month;
	    
	    @Column(name="o_sms_count_year")
	    private int o_sms_count_year;
	    
	    @Column(name="t_voice_callCount_day")
	    private int t_voice_callCount_day;
	    
	    @Column(name="t_voice_callCount_week")
	    private int t_voice_callCount_week;
	    
	    @Column(name="t_voice_callCount_month")
	    private int t_voice_callCount_month;
	    
	    @Column(name="t_voice_callCount_year")
	    private int t_voice_callCount_year;
	    
	    @Column(name="t_sms_count_day")
	    private int t_sms_count_day;
	    
	    @Column(name="t_sms_count_week")
	    private int t_sms_count_week;
	    
	    @Column(name="t_sms_count_month")
	    private int t_sms_count_month;
	    
	    @Column(name="t_sms_count_year")
	    private int t_sms_count_year;
	    
	    
	    @Column(name="date_time")
	    private Timestamp date_time;
	    
	    @Column(name="action")
	    private String action;
	    
	    @CreationTimestamp
	    @Column(name="action_DateTime")
		private Timestamp action_DateTime;
	    

	   
	    public PUCCallDetailsLog() {
	        super();
	
	     
	    }
	   
        

		public Long getMsisdn() {
			return msisdn;
		}



		public void setMsisdn(Long msisdn) {
			this.msisdn = msisdn;
		}


		public int getO_voice_callCount_day() {
			return o_voice_callCount_day;
		}



		public void setO_voice_callCount_day(int o_voice_callCount_day) {
			this.o_voice_callCount_day = o_voice_callCount_day;
		}



		public int getO_voice_callCount_week() {
			return o_voice_callCount_week;
		}



		public void setO_voice_callCount_week(int o_voice_callCount_week) {
			this.o_voice_callCount_week = o_voice_callCount_week;
		}



		public int getO_voice_callCount_month() {
			return o_voice_callCount_month;
		}



		public void setO_voice_callCount_month(int o_voice_callCount_month) {
			this.o_voice_callCount_month = o_voice_callCount_month;
		}



		public int getO_voice_callCount_year() {
			return o_voice_callCount_year;
		}



		public void setO_voice_callCount_year(int o_voice_callCount_year) {
			this.o_voice_callCount_year = o_voice_callCount_year;
		}



		public int getT_voice_callCount_day() {
			return t_voice_callCount_day;
		}



		public void setT_voice_callCount_day(int t_voice_callCount_day) {
			this.t_voice_callCount_day = t_voice_callCount_day;
		}



		public int getT_voice_callCount_week() {
			return t_voice_callCount_week;
		}



		public void setT_voice_callCount_week(int t_voice_callCount_week) {
			this.t_voice_callCount_week = t_voice_callCount_week;
		}



		public int getT_voice_callCount_month() {
			return t_voice_callCount_month;
		}



		public void setT_voice_callCount_month(int t_voice_callCount_month) {
			this.t_voice_callCount_month = t_voice_callCount_month;
		}



		public int getT_voice_callCount_year() {
			return t_voice_callCount_year;
		}



		public void setT_voice_callCount_year(int t_voice_callCount_year) {
			this.t_voice_callCount_year = t_voice_callCount_year;
		}



		public int getT_sms_count_week() {
			return t_sms_count_week;
		}



		public void setT_sms_count_week(int t_sms_count_week) {
			this.t_sms_count_week = t_sms_count_week;
		}



		public int getT_sms_count_month() {
			return t_sms_count_month;
		}



		public void setT_sms_count_month(int t_sms_count_month) {
			this.t_sms_count_month = t_sms_count_month;
		}



		public int getT_sms_count_year() {
			return t_sms_count_year;
		}



		public void setT_sms_count_year(int t_sms_count_year) {
			this.t_sms_count_year = t_sms_count_year;
		}



		public Timestamp getDate_time() {
			return date_time;
		}



		public void setDate_time(Timestamp date_time) {
			this.date_time = date_time;
		}

       

		public Long getId() {
			return id;
		}



		public void setId(Long id) {
			this.id = id;
		}



		public int getO_sms_count_day() {
			return o_sms_count_day;
		}



		public void setO_sms_count_day(int o_sms_count_day) {
			this.o_sms_count_day = o_sms_count_day;
		}



		public int getO_sms_count_week() {
			return o_sms_count_week;
		}



		public void setO_sms_count_week(int o_sms_count_week) {
			this.o_sms_count_week = o_sms_count_week;
		}



		public int getO_sms_count_month() {
			return o_sms_count_month;
		}



		public void setO_sms_count_month(int o_sms_count_month) {
			this.o_sms_count_month = o_sms_count_month;
		}



		public int getO_sms_count_year() {
			return o_sms_count_year;
		}



		public void setO_sms_count_year(int o_sms_count_year) {
			this.o_sms_count_year = o_sms_count_year;
		}



		public int getT_sms_count_day() {
			return t_sms_count_day;
		}



		public void setT_sms_count_day(int t_sms_count_day) {
			this.t_sms_count_day = t_sms_count_day;
		}



		public String getAction() {
			return action;
		}


		public void setAction(String action) {
			this.action = action;
		}


		public Timestamp getAction_DateTime() {
			return action_DateTime;
		}


		public void setAction_DateTime(Timestamp action_DateTime) {
			this.action_DateTime = action_DateTime;
		}


		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((action == null) ? 0 : action.hashCode());
			result = prime * result + ((action_DateTime == null) ? 0 : action_DateTime.hashCode());
			result = prime * result + ((date_time == null) ? 0 : date_time.hashCode());
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			result = prime * result + ((msisdn == null) ? 0 : msisdn.hashCode());
			result = prime * result + o_sms_count_day;
			result = prime * result + o_sms_count_month;
			result = prime * result + o_sms_count_week;
			result = prime * result + o_sms_count_year;
			result = prime * result + o_voice_callCount_day;
			result = prime * result + o_voice_callCount_month;
			result = prime * result + o_voice_callCount_week;
			result = prime * result + o_voice_callCount_year;
			result = prime * result + t_sms_count_day;
			result = prime * result + t_sms_count_month;
			result = prime * result + t_sms_count_week;
			result = prime * result + t_sms_count_year;
			result = prime * result + t_voice_callCount_day;
			result = prime * result + t_voice_callCount_month;
			result = prime * result + t_voice_callCount_week;
			result = prime * result + t_voice_callCount_year;
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PUCCallDetailsLog other = (PUCCallDetailsLog) obj;
			if (action == null) {
				if (other.action != null)
					return false;
			} else if (!action.equals(other.action))
				return false;
			if (action_DateTime == null) {
				if (other.action_DateTime != null)
					return false;
			} else if (!action_DateTime.equals(other.action_DateTime))
				return false;
			if (date_time == null) {
				if (other.date_time != null)
					return false;
			} else if (!date_time.equals(other.date_time))
				return false;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			if (msisdn == null) {
				if (other.msisdn != null)
					return false;
			} else if (!msisdn.equals(other.msisdn))
				return false;
			if (o_sms_count_day != other.o_sms_count_day)
				return false;
			if (o_sms_count_month != other.o_sms_count_month)
				return false;
			if (o_sms_count_week != other.o_sms_count_week)
				return false;
			if (o_sms_count_year != other.o_sms_count_year)
				return false;
			if (o_voice_callCount_day != other.o_voice_callCount_day)
				return false;
			if (o_voice_callCount_month != other.o_voice_callCount_month)
				return false;
			if (o_voice_callCount_week != other.o_voice_callCount_week)
				return false;
			if (o_voice_callCount_year != other.o_voice_callCount_year)
				return false;
			if (t_sms_count_day != other.t_sms_count_day)
				return false;
			if (t_sms_count_month != other.t_sms_count_month)
				return false;
			if (t_sms_count_week != other.t_sms_count_week)
				return false;
			if (t_sms_count_year != other.t_sms_count_year)
				return false;
			if (t_voice_callCount_day != other.t_voice_callCount_day)
				return false;
			if (t_voice_callCount_month != other.t_voice_callCount_month)
				return false;
			if (t_voice_callCount_week != other.t_voice_callCount_week)
				return false;
			if (t_voice_callCount_year != other.t_voice_callCount_year)
				return false;
			return true;
		}    
	
	}
