package com.digispice.puc.dto;
import java.io.Serializable;
import java.sql.Timestamp;
import com.digispice.puc.entity.ProductInfo;
import com.digispice.puc.deserializers.CustomDeSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


@JsonDeserialize(using=CustomDeSerializer.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(value= {"category",  "status", "username"}, allowSetters=true)
public class WhiteListMsisdnDto implements Serializable  {

   
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("msisdn")
    private Long msisdn;
    private String product;
    private String version;
    private Timestamp release;
    private Long category;
    private String username;
    private int status;
    private ProductInfo apiInfo;
    private Long imsi;
    private String createdby;
  

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public ProductInfo getApiInfo() {
		return apiInfo;
	}

	public void setApiInfo(ProductInfo apiInfo) {
		this.apiInfo = apiInfo;
	}

	public Long getMsisdn() {
		return msisdn;
	}
    
	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}
	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Timestamp getRelease() {
		return release;
	}

	public void setRelease(Timestamp release) {
		this.release = release;
	}

	public WhiteListMsisdnDto() {
        super();

    }

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	public Long getCategory() {
		return category;
	}
	public void setCategory(Long category) {
		this.category = category;
	}


	public Long getImsi() {
		return imsi;
	}

	public void setImsi(Long imsi) {
		this.imsi = imsi;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}  
	
    
}
