package com.digispice.puc.service;
import com.digispice.puc.dto.WhiteListMasterWrapperDto;
import com.digispice.puc.dto.WhiteListMsisdnDto;
import com.digispice.puc.model.ApiResponsePojo;

	public interface UserService {
	
	    public boolean existsById(long msisdn);
	    public ApiResponsePojo deleteUserByMsisdn(WhiteListMasterWrapperDto resource);
	    public ApiResponsePojo createUser(final WhiteListMsisdnDto resource);
	    public ApiResponsePojo findMsisdn(WhiteListMasterWrapperDto resource);
	    public int countByMsisdn(long msisdn);
		public ApiResponsePojo addUserWrapper(WhiteListMsisdnDto resource);
	
	   	    
	}


