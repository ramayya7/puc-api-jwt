package com.digispice.puc.model;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;
import com.digispice.puc.serializers.ApiResponseSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using= ApiResponseSerializer.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponsePojo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @JsonIgnore
    private int status;
    private String message;
    private String msisdn;
    private String returnCode;
    private String error;
    private Timestamp release;
    private String product;
    private String version;
    private String action;
    private String imsi;
    private String remarks;
    private String description;
   

    public ApiResponsePojo() {
    	
    }
   
   
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

   
    public ApiResponsePojo(final String msisdn, final String returnCode,final String remarks, final String description) {
        
    	this.msisdn = msisdn;
        this.returnCode = returnCode;
        this.remarks=remarks;
        this.description=description;
  
    }
    
    public ApiResponsePojo(final int status, final String error) {
        this.status = status;
        this.error = error;
  
    }
    
    public ApiResponsePojo(final String error) {
        this.error = error;
        
  
    }
    
    
    public ApiResponsePojo(final String msisdn, final int status, final String returnCode, final String remarks, final String description) {
        this.msisdn=msisdn;
    	this.status = status;
        this.returnCode=returnCode;
        this.remarks=remarks;
        this.description=description;
     
    }
    

    public ApiResponsePojo(String msisdn, int status, String returnCode, String remarks, String description, String action) {
      this.msisdn = msisdn;
      this.status = status;
      this.returnCode = returnCode;
      this.remarks = remarks;
      this.description = description;
      this.action = action;
    }


  
    public int getStatus() {
        return status;
    }

    public void setStatus(final int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }


    @Override
    public final String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("ApiError [status=").append(status).append(", message=").append(message).append("]");
        return builder.toString();
    }

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}
	public Timestamp getRelease() {
		return release;
	}
	public void setRelease(Timestamp release) {
		this.release = release;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
    }

