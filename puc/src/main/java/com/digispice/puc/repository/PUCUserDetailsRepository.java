package com.digispice.puc.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.digispice.puc.entity.PUCUserDetails;

public interface PUCUserDetailsRepository extends JpaRepository<PUCUserDetails, Long> {
	
	public PUCUserDetails findByName(final String name);

}


