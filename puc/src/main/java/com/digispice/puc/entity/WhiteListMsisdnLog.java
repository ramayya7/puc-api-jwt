package com.digispice.puc.entity;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;


@Entity
@Table(name="tbl_whitelist_msisdn_log")
public class WhiteListMsisdnLog implements Serializable  {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    @Column(name="name")
    private String name;
    @Column(name="msisdn")
    private Long msisdn;
	@Column(name="status")
	private int status;
	@Column(name="creationDateTime")
    private Timestamp creationDateTime;
   
	@Column(name="updateDateTime")
    private Timestamp updateDateTime;
    @Column(name="imsi")
	private Long imsi;
    @Column(name="action")
    private String action;
    @CreationTimestamp
    @Column(name="action_DateTime")
	private Timestamp action_DateTime;
    
    public Long getImsi() {
		return imsi;
	}
    
    public Timestamp getCreationDateTime() {
		return creationDateTime;
	}

	public void setCreationDateTime(Timestamp creationDateTime) {
		this.creationDateTime = creationDateTime;
	}
	
	public void setImsi(Long imsi) {
		this.imsi = imsi;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Timestamp getAction_DateTime() {
		return action_DateTime;
	}

	public void setAction_DateTime(Timestamp action_DateTime) {
		this.action_DateTime = action_DateTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	
	
	public Timestamp getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Timestamp updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
	
 
	public Long getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}


	public WhiteListMsisdnLog() {
        super();

     
    }

	
}
