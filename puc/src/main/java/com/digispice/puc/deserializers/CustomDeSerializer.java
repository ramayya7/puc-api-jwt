package com.digispice.puc.deserializers;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.digispice.puc.dto.WhiteListMsisdnDto;
import com.digispice.puc.exception.models.BadRequestException;
import com.digispice.puc.utilities.RestPreconditions;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

	public class CustomDeSerializer extends StdDeserializer<WhiteListMsisdnDto> {
		
		private Logger logger = LoggerFactory.getLogger(getClass());
		
		private static final long serialVersionUID = 1L;
		
		private static WhiteListMsisdnDto whiteListMsisdnDto =new WhiteListMsisdnDto();
		
		private String exceptionParam=null;
		
		
		protected CustomDeSerializer(Class<WhiteListMsisdnDto> t) {
		    super(t);
		}
		@SuppressWarnings("unchecked")
		public CustomDeSerializer() {
			this((Class<WhiteListMsisdnDto>) whiteListMsisdnDto.getClass());     
		}
		public  WhiteListMsisdnDto deserialize(JsonParser parser, DeserializationContext deserializer) throws IOException {
			try
			{    
				 
					
					ObjectMapper objectMapper = new ObjectMapper();
					
				
					JsonNode rootNode = objectMapper.readTree(parser);
					
				    logger.info("Deserializing Add User Block");
		                
		            JsonNode name = rootNode.path("username");
		                
		            JsonNode category =rootNode.path("category");
		                
                    JsonNode msisdn = rootNode.path("msisdn");
                        
                    logger.info("Msisdn["+msisdn.textValue().trim()+"]");
		                
                     JsonNode imsi = rootNode.path("imsi");
		                
		             if(msisdn.textValue()==null || msisdn.textValue()=="")
			         {				              
				              throw new BadRequestException(msisdn.textValue(), "Bad Request Format");
			         }
			            
			         else if(msisdn.textValue().trim()!=null)
			         { 	
			            	
			            	logger.info("Msisdn["+msisdn.textValue().trim()+"]");
			            	
			                RestPreconditions.checkRequestElementIsValid(msisdn.textValue().trim(), "Bad Request Format");
			         }
		                
			           
			          whiteListMsisdnDto.setMsisdn(msisdn.asLong());
			            
						
			          if (category.textValue() == null || category.textValue() != "") 
			          {
			              
			              whiteListMsisdnDto.setCategory(Long.valueOf(1L));
			          } 
			          else
			          {
			              
			              if (category.textValue() != null && category.textValue() != "" && category.asLong() == 0L)
			              {
			                
			                throw new BadRequestException(msisdn.textValue().trim(), "Bad Request Format");
			              }
			              
			              if (category.textValue() != null && category.textValue() != "" && category.asLong() < 0L)
			              {
			                
			                throw new BadRequestException(msisdn.textValue().trim(), "Bad Request Format");
			              }

			              whiteListMsisdnDto.setCategory(Long.valueOf(category.asLong()));
			          } 
		                
		                
		                if(name.textValue()==null || name.textValue()=="")
		                {	
		                	whiteListMsisdnDto.setUsername("VIL");
		                }
		                
		                else if (name.textValue() != null && name.textValue() != "") {
		                    whiteListMsisdnDto.setUsername(name.textValue());
		                 }
		                
		                else
		                {
		                	whiteListMsisdnDto.setUsername(name.textValue());
		                }
		               
		                
		                if(imsi.textValue()!=null && imsi.textValue()!="" && imsi.textValue().matches("\\d+") )
			            {	
		                	whiteListMsisdnDto.setImsi(imsi.asLong());
			            }
		                
		                else if(imsi.textValue()==null || imsi.textValue()=="")
			            {	
		                	whiteListMsisdnDto.setImsi(Long.valueOf(0));
			            }
			            
			            else if(imsi.textValue()!=null && !(imsi.textValue().matches("\\d+")))
			            {	
			            	 throw new BadRequestException(msisdn.textValue().trim(), "Bad Request Format");
				        }
		            
			            else
			            {
			            	whiteListMsisdnDto.setImsi(Long.valueOf(0));
			            }
			            					
					
			}
		   
	        catch(JsonParseException e)
			
			{   
	        		
				logger.error("Error["+e+"]");
				
			    throw new BadRequestException(exceptionParam, "Bad Request Format");
			    
			}
			
			return whiteListMsisdnDto;
		}
	
	
		
		}
		


