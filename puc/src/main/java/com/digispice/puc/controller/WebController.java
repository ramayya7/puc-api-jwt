package com.digispice.puc.controller;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.digispice.puc.dto.WhiteListMasterWrapperDto;
import com.digispice.puc.dto.WhiteListMsisdnDto;
import com.digispice.puc.exception.models.ResourceNotFoundException;
import com.digispice.puc.model.ApiResponsePojo;
import com.digispice.puc.service.impl.UserServiceImpl;
import com.digispice.puc.utilities.RestPreconditions;

@RestController
@RequestMapping("/users/")
public class WebController {

	protected final Log logger = LogFactory.getLog(getClass());
	
	private ResponseEntity<Object> responseEntity=null;
	
	@Autowired 
	private UserServiceImpl userService;
	
	private ApiResponsePojo apiResponsePojo;


	@PostMapping(value = {"details"}, consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public ResponseEntity<Object> fetchUser(@Valid @RequestBody WhiteListMasterWrapperDto resource) {
	    
		RestPreconditions.checkRequestElementNotNull(resource);
	    
	    apiResponsePojo = userService.findMsisdn(resource);
	    
	    responseEntity = new ResponseEntity<Object>(apiResponsePojo, HttpStatus.OK);
	    
	    return responseEntity;
	 }
	
	 @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	 @Secured("ROLE_ADMIN")
	 public ResponseEntity<Object> delete(@Valid @RequestBody WhiteListMasterWrapperDto resource) throws HttpRequestMethodNotSupportedException
	   
	 {
		
	        try
	        {       
	        	  
	        	    RestPreconditions.checkRequestElementNotNull(resource);
		        	
		        	ApiResponsePojo responsePojo = userService.deleteUserByMsisdn(resource);
		        	
		        	responseEntity =new ResponseEntity<Object>(responsePojo,HttpStatus.OK);
		        	
		            return responseEntity;
	        }
	       
	        catch(ResourceNotFoundException e) 
	        {
	           throw new ResourceNotFoundException("Msisdn is not present in the system", resource.getMsisdn().toString());
	        }
	   }
	  	
		@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	    @Secured("ROLE_ADMIN")
	    public  ResponseEntity<Object> createUser(@Valid @RequestBody final WhiteListMsisdnDto resource) throws HttpRequestMethodNotSupportedException
		
		{
			
	    	 RestPreconditions.checkRequestElementNotNull(resource);
	    	 
	    	 apiResponsePojo= userService.createUser(resource); 
	    	 
	    	 if(apiResponsePojo.getReturnCode().equalsIgnoreCase("success"))
	    	 {		 
	            responseEntity =new ResponseEntity<Object>(apiResponsePojo,HttpStatus.CREATED);
	    	 }
	    	 	 
	    	 return responseEntity;
			
	    	
	    }

}