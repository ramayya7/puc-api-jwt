package com.digispice.puc.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import com.digispice.puc.entity.PUCCallDetails;



public interface PUCCallDetailsRepository extends JpaRepository<PUCCallDetails, Long>  {
    
	@Query("SELECT p FROM PUCCallDetails p where p.msisdn = :msisdn")
	List<PUCCallDetails> findMsisdn(@Param("msisdn") Long msisdn);
	
    @Transactional
    int deleteByMsisdn(@Param("msisdn") Long msisdn);
    
    int countByMsisdn(@Param("msisdn") Long msisdn);
   
    public boolean existsById(@Param("msisdn") Long msisdn);
    
    
}
