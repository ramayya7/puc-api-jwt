package com.digispice.puc.service.impl;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import com.digispice.puc.beans.AuthenticationFacade;
import com.digispice.puc.dto.WhiteListMasterWrapperDto;
import com.digispice.puc.dto.WhiteListMsisdnDto;
import com.digispice.puc.exception.models.ResourceNotFoundException;
import com.digispice.puc.model.ApiResponsePojo;
import com.digispice.puc.entity.PUCCallDetails;
import com.digispice.puc.entity.PUCCallDetailsLog;
import com.digispice.puc.entity.ProductInfo;
import com.digispice.puc.entity.WhiteListMsisdn;
import com.digispice.puc.entity.WhiteListMsisdnLog;
import com.digispice.puc.repository.PUCCallDetailsLogRepository;
import com.digispice.puc.repository.PUCCallDetailsRepository;
import com.digispice.puc.repository.ProductInfoRepository;
import com.digispice.puc.repository.WhiteListMsisdnLogRepository;
import com.digispice.puc.repository.WhiteListMsisdnRepository;
import com.digispice.puc.service.UserService;
import com.digispice.puc.utilities.RestPreconditions;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService{
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	WhiteListMsisdnRepository  whiteListMsisdnRepository;
		
	@Autowired
	WhiteListMsisdnLogRepository whitelistedMsisdnLogRepository;
	
	@Autowired
	PUCCallDetailsRepository pucCallDetailsRepository;
	
	@Autowired
	PUCCallDetailsLogRepository pucCallDetailsLogRepository;
	
	@Autowired
	ProductInfoRepository productInfoRepository;
	
	@Autowired
	ProductInfoServiceImpl productInfoServiceImpl;
	
	@Autowired
	private ModelMapper modelmapper;
	
	@Autowired
    private AuthenticationFacade authenticationFacade;
	   
	private ApiResponsePojo apiResponsePojo=null;
   
    private ProductInfo apiInfo=null;
	
    @PostConstruct
    public void postConstruct()
    {
      apiInfo = productInfoServiceImpl.fetchProductDetails();
      
      logger.info("API Info Loaded Into Memory");
    }
	
	@Override
	@Transactional(readOnly = true)
	public ApiResponsePojo findMsisdn(WhiteListMasterWrapperDto resource) {
	
    int count = countByMsisdn(resource.getMsisdn().longValue());
      
    if(count==0)
    {        
    	     logger.error("Msisdn is not present in the system");
    	     

    	     throw new ResourceNotFoundException("Msisdn is not present in the system", Long.toString(resource.getMsisdn().longValue()));
    	     
    }
    else
    {
    	    
    	    List<WhiteListMsisdnDto> whiteListedUserDetails=whiteListMsisdnRepository.findMsisdn(resource.getMsisdn()).stream().map(this::convertToDto).collect(Collectors.toList());
    	    
			apiResponsePojo=responseBuilder(whiteListedUserDetails,apiInfo);
			
			return apiResponsePojo;	
	}
    
	}
	
	@Override
	public ApiResponsePojo deleteUserByMsisdn(WhiteListMasterWrapperDto resource) {
		
		
        try{
        	
        	int rowsDeleted=0;
        	int count=countByMsisdn(resource.getMsisdn().longValue());
        	
    		if(count==0)
        	{   

    	        throw new ResourceNotFoundException("Msisdn is not present in the system", Long.toString(resource.getMsisdn().longValue()));
        	}
        	else
        	{   
        		List<WhiteListMsisdn> userDetails=  (List<WhiteListMsisdn>) whiteListMsisdnRepository.findMsisdn(resource.getMsisdn());
        		List<PUCCallDetails> pucCallDetails=  (List<PUCCallDetails>) pucCallDetailsRepository.findMsisdn(resource.getMsisdn());
        	    List<WhiteListMsisdnLog> whitelistMsisdnLog = userDetails.stream().map(w->
    	        		                                     { 
							        		                      WhiteListMsisdnLog whiteListedUserLogs=new WhiteListMsisdnLog();
							        		                      whiteListedUserLogs.setName(w.getUsername());
							        		                      whiteListedUserLogs.setMsisdn(w.getMsisdn());
							        		                      whiteListedUserLogs.setImsi(w.getImsi());
							        		                      whiteListedUserLogs.setStatus(w.getStatus());
							        		                      whiteListedUserLogs.setCreationDateTime(w.getCreationDateTime());
							        		                      whiteListedUserLogs.setUpdateDateTime(w.getUpdateDateTime());
							    			                      return whiteListedUserLogs;
        	                                                 }).collect(Collectors.toList());
        	    if(pucCallDetails.size()>0)
        	    { 	
        	      List<PUCCallDetailsLog> pucallDetailsLog = pucCallDetails.stream().map(p->
                  { 
                	 PUCCallDetailsLog pucUserCallDetailsLogs=new PUCCallDetailsLog();
                	 
                	 pucUserCallDetailsLogs.setMsisdn(p.getMsisdn());
                	 pucUserCallDetailsLogs.setO_voice_callCount_day(p.getO_voice_callCount_day());
                	 pucUserCallDetailsLogs.setO_voice_callCount_week(p.getO_voice_callCount_week());
                	 pucUserCallDetailsLogs.setO_voice_callCount_month(p.getO_voice_callCount_month());
                	 pucUserCallDetailsLogs.setO_voice_callCount_year(p.getO_voice_callCount_year());
                	 pucUserCallDetailsLogs.setO_sms_count_day(p.getO_sms_count_day());
                	 pucUserCallDetailsLogs.setO_sms_count_week(p.getO_sms_count_week());
                	 pucUserCallDetailsLogs.setO_sms_count_week(p.getO_sms_count_week());
                	 pucUserCallDetailsLogs.setO_sms_count_month(p.getO_sms_count_month());
                	 pucUserCallDetailsLogs.setO_sms_count_year(p.getO_sms_count_year());
                	 pucUserCallDetailsLogs.setT_voice_callCount_day(p.getT_voice_callCount_day());
                	 pucUserCallDetailsLogs.setT_voice_callCount_week(p.getT_voice_callCount_week());
                	 pucUserCallDetailsLogs.setT_voice_callCount_month(p.getT_voice_callCount_month());
                	 pucUserCallDetailsLogs.setT_voice_callCount_year(p.getT_voice_callCount_year());
                	 pucUserCallDetailsLogs.setT_sms_count_day(p.getT_sms_count_day());
                	 pucUserCallDetailsLogs.setT_sms_count_week(p.getT_sms_count_week());
                	 pucUserCallDetailsLogs.setT_sms_count_month(p.getT_sms_count_month());
                	 pucUserCallDetailsLogs.setT_sms_count_year(p.getT_sms_count_year());
                	 pucUserCallDetailsLogs.setDate_time(p.getDate_time());
                     return pucUserCallDetailsLogs;
                    }).collect(Collectors.toList());
        	      
        	         PUCCallDetailsLog pucUserLog=new PUCCallDetailsLog();
        	         
	     		     pucUserLog.setMsisdn(pucallDetailsLog.get(0).getMsisdn());
	     		     pucUserLog.setO_voice_callCount_day(pucallDetailsLog.get(0).getO_voice_callCount_day());
	     		     pucUserLog.setO_voice_callCount_week(pucallDetailsLog.get(0).getO_voice_callCount_week());
	     		     pucUserLog.setO_voice_callCount_month(pucallDetailsLog.get(0).getO_voice_callCount_month());
	     		     pucUserLog.setO_voice_callCount_year(pucallDetailsLog.get(0).getO_voice_callCount_year());
	     		     pucUserLog.setO_sms_count_day(pucallDetailsLog.get(0).getO_sms_count_day());
	     		     pucUserLog.setO_sms_count_week(pucallDetailsLog.get(0).getO_sms_count_week());
	     		     pucUserLog.setO_sms_count_week(pucallDetailsLog.get(0).getO_sms_count_week());
	 	           	 pucUserLog.setO_sms_count_month(pucallDetailsLog.get(0).getO_sms_count_month());
	 	           	 pucUserLog.setO_sms_count_year(pucallDetailsLog.get(0).getO_sms_count_year());
	 	           	 pucUserLog.setT_voice_callCount_day(pucallDetailsLog.get(0).getT_voice_callCount_day());
	 	           	 pucUserLog.setT_voice_callCount_week(pucallDetailsLog.get(0).getT_voice_callCount_week());
	 	           	 pucUserLog.setT_voice_callCount_month(pucallDetailsLog.get(0).getT_voice_callCount_month());
	 	           	 pucUserLog.setT_voice_callCount_year(pucallDetailsLog.get(0).getT_voice_callCount_year());
	 	           	 pucUserLog.setT_sms_count_day(pucallDetailsLog.get(0).getT_sms_count_day());
	 	           	 pucUserLog.setT_sms_count_week(pucallDetailsLog.get(0).getT_sms_count_week());
	 	           	 pucUserLog.setT_sms_count_month(pucallDetailsLog.get(0).getT_sms_count_month());
	 	           	 pucUserLog.setT_sms_count_year(pucallDetailsLog.get(0).getT_sms_count_year());
	 	           	 pucUserLog.setDate_time(pucallDetailsLog.get(0).getDate_time());
	 	           	 pucUserLog.setAction("deleteUser");  
	 	           
	 	           	PUCCallDetailsLog pucCallDetailsLogObj =pucCallDetailsLogRepository.saveAndFlush(pucUserLog);  
	    	        
        	    }
        	    
    	    	WhiteListMsisdnLog userLog=new WhiteListMsisdnLog();
    	        userLog.setName(whitelistMsisdnLog.get(0).getName());
    		    userLog.setMsisdn(whitelistMsisdnLog.get(0).getMsisdn());
    	        userLog.setStatus(whitelistMsisdnLog.get(0).getStatus());
    	        userLog.setCreationDateTime(whitelistMsisdnLog.get(0).getCreationDateTime());
    	        userLog.setUpdateDateTime(userDetails.get(0).getUpdateDateTime());
    		    userLog.setImsi(userDetails.get(0).getImsi());
    		    userLog.setAction("deleteUser");
    		    
                WhiteListMsisdnLog whitelistMsisdnLogObj =whitelistedMsisdnLogRepository.saveAndFlush(userLog);   	   	    	
    	  
    	    	if (whitelistMsisdnLogObj!=null)
    	    	{ 	
    	    		rowsDeleted=whiteListMsisdnRepository.deleteByMsisdn(resource.getMsisdn());
            	
    	    	}
        		
        		if(rowsDeleted>0)
        		{
        			
        			apiResponsePojo =new ApiResponsePojo(String.valueOf(resource.getMsisdn()),HttpStatus.OK.value(),"success","deleted","Requested number has been deleted from the system","deleteUser"); 
        			
        	}
        		
        	}
        	return apiResponsePojo;
        }
        catch(ResourceNotFoundException e) {
        throw new ResourceNotFoundException("Msisdn is not present in the system",  Long.toString(resource.getMsisdn().longValue()));
    
    }
		
	}
		
	@Override
	public ApiResponsePojo createUser(final WhiteListMsisdnDto resource)
	{   
	
		
	    RestPreconditions.checkRequestElementIsValid(resource.getMsisdn(), "Bad Request Format");
		
        
		int cnt=countByMsisdn(resource.getMsisdn());
		        
		RestPreconditions.checkEntityExists(cnt, resource.getMsisdn(), "Msisdn is present in the system");
		
		 Authentication authentication = authenticationFacade.getAuthentication();
		 
		 String username = authentication.getName();
		 
         logger.info("Username["+username+"]");
         
		 WhiteListMsisdnDto whitelistMsisdnDto=new WhiteListMsisdnDto();
		 whitelistMsisdnDto.setCreatedby(username);
		 whitelistMsisdnDto.setCategory(resource.getCategory());
		 whitelistMsisdnDto.setUsername(resource.getUsername());
		 whitelistMsisdnDto.setMsisdn(resource.getMsisdn());
		 whitelistMsisdnDto.setStatus(1);
	     whitelistMsisdnDto.setImsi(resource.getImsi());
	    
	     WhiteListMsisdn whitelistedUsers=whiteListMsisdnRepository.saveAndFlush(convertToEntity(whitelistMsisdnDto));
	   
         apiResponsePojo =new ApiResponsePojo();
     
         apiResponsePojo.setProduct(apiInfo.getProduct());
     
         apiResponsePojo.setVersion(apiInfo.getVersion());
     
         apiResponsePojo.setRelease(apiInfo.getRelease());
     
         apiResponsePojo.setMsisdn(resource.getMsisdn().toString());
     
         apiResponsePojo.setAction("addUser");
  
    	 logger.info("Msisdn has been whitelisted");
		      
		 apiResponsePojo.setReturnCode("success");
		      
		 apiResponsePojo.setRemarks("added");
    
		return apiResponsePojo;
		
    }
	
	@Override
	public ApiResponsePojo addUserWrapper(final WhiteListMsisdnDto resource)
	{    
		
		int msisdnCount=countByMsisdn(resource.getMsisdn());
		logger.info("Count["+msisdnCount+"]");
	   	
		if(msisdnCount==0)
		{	
			logger.info("Msisdn is not present in the system");
			//resource.setMsisdn(resource.getMsisdn());
			apiResponsePojo=createUser(resource);
		}
		else
		{
			//resource.setMsisdn(msisdn);
			RestPreconditions.checkEntityExists(msisdnCount, resource.getMsisdn(), "Msisdn is present in the system");
			
			
		}
		apiResponsePojo=new ApiResponsePojo();
		apiResponsePojo.setProduct(apiInfo.getProduct());
		apiResponsePojo.setVersion(apiInfo.getVersion());
		apiResponsePojo.setRelease(apiInfo.getRelease());
		apiResponsePojo.setMsisdn(resource.getMsisdn().toString());
		apiResponsePojo.setAction("updateUser");
		apiResponsePojo.setReturnCode("success");
        apiResponsePojo.setRemarks("added");
		return apiResponsePojo;
		
    }
	
	protected WhiteListMsisdn convertToEntity(WhiteListMsisdnDto dto) 
	{
		return this.modelmapper.map(dto, WhiteListMsisdn.class);
	}

	protected WhiteListMsisdnDto convertToDto(WhiteListMsisdn entity) 
	{
		return this.modelmapper.map(entity, WhiteListMsisdnDto.class);
	}

	@Override
	public boolean existsById(long msisdn) {

		boolean isMsisdnExists=whiteListMsisdnRepository.existsById(msisdn);
		logger.info("IsMsisdnExists["+isMsisdnExists+"]");
		return isMsisdnExists;
	}
	@Override
	public int countByMsisdn(long msisdn) {
		int cnt=whiteListMsisdnRepository.countByMsisdn(msisdn);
		logger.info("Count["+cnt+"]");
		return cnt;
	}

	public ApiResponsePojo responseBuilder(List<WhiteListMsisdnDto> userDetails, ProductInfo apiInfo)
	{   
		    
			apiResponsePojo =new ApiResponsePojo();
			
			apiResponsePojo.setProduct(apiInfo.getProduct());
			
			apiResponsePojo.setVersion(apiInfo.getVersion());
			
			apiResponsePojo.setRelease(apiInfo.getRelease());
			
			apiResponsePojo.setMsisdn(userDetails.get(0).getMsisdn().toString());
			
			apiResponsePojo.setAction("getUser");
			
	        apiResponsePojo.setImsi(userDetails.get(0).getImsi().toString());
		    
	        return apiResponsePojo;	
	}
	
	

}
