package com.digispice.puc.entity;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name="tbl_whitelist_msisdn")

public class WhiteListMsisdn implements Serializable  {

    private static final long serialVersionUID = 1L;
   
    @Id
	@Column(name="msisdn")
    private Long msisdn;
    
    @Column(name="name")
	private String username;
    
    @Column(name="category")
    private Long category;
    
	@Column(name="status")
	private int status;
	
	@Column(name="imsi")
	private Long imsi;
	
	@CreationTimestamp
	@Column(name="creationDateTime")
	private Timestamp creationDateTime;
	
	@UpdateTimestamp
	@Column(name="updateDateTime")
	private Timestamp updateDateTime;
	
	@Column(name="createdby")
	private String createdby;
	
	@OneToMany(mappedBy="whitelistMsisdnMaster",fetch=FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval=true)
	private Set<PUCCallDetails> pucCallDetails;
	
	
	public Set<PUCCallDetails> getPucCallDetails() {
		return pucCallDetails;
	}

	public void setPucCallDetails(Set<PUCCallDetails> pucCallDetails) {
		this.pucCallDetails = pucCallDetails;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Timestamp getCreationDateTime() {
		return creationDateTime;
	}

	public void setCreationDateTime(Timestamp creationDateTime) {
		this.creationDateTime = creationDateTime;
	}


	public Long getCategory() {
		return category;
	}

	public void setCategory(Long category) {
		this.category = category;
	}

	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Timestamp getUpdateDateTime() {
		return updateDateTime;
	}
	
	public Long getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}


	public void setUpdateDateTime(Timestamp updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public Long getImsi() {
		return imsi;
	}

	public void setImsi(Long imsi) {
		this.imsi = imsi;
	}
     
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public WhiteListMsisdn() {
        super();

     
    }

	
}
