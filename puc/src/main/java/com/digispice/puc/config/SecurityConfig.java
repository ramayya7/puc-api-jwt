package com.digispice.puc.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import com.digispice.puc.security.CustomUserDetailsService;
import com.digispice.puc.security.RestAuthenticationEntryPoint;
import org.springframework.security.crypto.password.MessageDigestPasswordEncoder;

@Configuration
@EnableResourceServer
@EnableWebSecurity
@ComponentScan({"com.digispice.puc.security"})
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends ResourceServerConfigurerAdapter  {
      
    @Autowired
    private CustomUserDetailsService userDetailsService;
    
    @Value("${signing-key:oui214hmui23o4hm1pui3o2hp4m1o3h2m1o43}")
    private String signingKey;
    

    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
    	
    	 auth.authenticationProvider(authenticationProvider());
    }

   
    public void configure(final HttpSecurity http) throws Exception {
      
    	
		 http
		.csrf().disable()
	    .authorizeRequests()
	    .antMatchers("/puc/login*").permitAll()
	    .antMatchers("/puc/outh/access-token/").permitAll()
		.antMatchers("/puc/users/**").
		 authenticated().
		 and()
		.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())
		.and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	 
      
    }
    @Bean
    public BasicAuthenticationEntryPoint authenticationEntryPoint(){
    	
    	RestAuthenticationEntryPoint entryPoint = new RestAuthenticationEntryPoint();
        return entryPoint;
    }
    
  
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
    	
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }
     
    @Bean
    public PasswordEncoder passwordEncoder() {
    
       return  new MessageDigestPasswordEncoder("SHA-256");
    }
}
