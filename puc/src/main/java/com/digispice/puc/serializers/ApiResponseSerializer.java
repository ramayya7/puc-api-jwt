package com.digispice.puc.serializers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.digispice.puc.model.ApiResponsePojo;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;


public class ApiResponseSerializer extends StdSerializer<ApiResponsePojo> {

	private static ApiResponsePojo responsePojo =new ApiResponsePojo();
	
	@SuppressWarnings("unchecked")
	public ApiResponseSerializer() {
		this((Class<ApiResponsePojo>)responsePojo.getClass());     
	}
	
	protected ApiResponseSerializer(Class<ApiResponsePojo> t) {
		super(t);

	}

	private Logger logger = LoggerFactory.getLogger(getClass());
	private static final long serialVersionUID = 1L;
	
	@Override
	public void serialize(ApiResponsePojo apiResponsePojo, JsonGenerator jsonGenerator, SerializerProvider provider) {
		try
		{   
			
		
			jsonGenerator.writeStartObject();

		    logger.info("Action[" + apiResponsePojo.getAction() + "]");
			
			 if (!apiResponsePojo.getAction().equalsIgnoreCase("getUser") && !apiResponsePojo.getAction().equalsIgnoreCase("deleteUser"))
		      {
		        
		        jsonGenerator.writeStringField("product", apiResponsePojo.getProduct());
		        jsonGenerator.writeStringField("version", apiResponsePojo.getVersion());
		        jsonGenerator.writeStringField("release", apiResponsePojo.getRelease().toString());
		        jsonGenerator.writeStringField("msisdn", apiResponsePojo.getMsisdn());
		        jsonGenerator.writeStringField("returnCode", apiResponsePojo.getReturnCode());
		        jsonGenerator.writeStringField("remarks", apiResponsePojo.getRemarks());
		        jsonGenerator.writeEndObject();

		      
		      }
			  
			 else if (apiResponsePojo.getAction().equalsIgnoreCase("getUser"))
		      {
		        
		        jsonGenerator.writeStringField("product", apiResponsePojo.getProduct());
		        jsonGenerator.writeStringField("version", apiResponsePojo.getVersion());
		        jsonGenerator.writeStringField("release", apiResponsePojo.getRelease().toString());
		        jsonGenerator.writeStringField("msisdn", apiResponsePojo.getMsisdn());
		        jsonGenerator.writeStringField("imsi", apiResponsePojo.getImsi());
		        jsonGenerator.writeEndObject();
		      
		      }
			
			 else
		      {
		        
		        logger.info("Else Block In Response Serializer:");
		        jsonGenerator.writeStringField("msisdn", apiResponsePojo.getMsisdn());
		        jsonGenerator.writeStringField("returnCode", apiResponsePojo.getReturnCode());
		        jsonGenerator.writeStringField("remarks", apiResponsePojo.getRemarks());
		        jsonGenerator.writeStringField("description", apiResponsePojo.getDescription());
		        jsonGenerator.writeEndObject();
		      
		      }
								
				
			jsonGenerator.writeEndObject();
		}

		catch(Exception e)

		{  
			logger.error("Error["+e+"]");
			

		}

	} 

}