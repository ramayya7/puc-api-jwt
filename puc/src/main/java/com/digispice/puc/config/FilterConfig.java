package com.digispice.puc.config;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "com.digispice.puc.filter" })
public class FilterConfig {

    public FilterConfig() {
        super();
    }
   
    
}
